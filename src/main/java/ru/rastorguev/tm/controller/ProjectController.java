package ru.rastorguev.tm.controller;

import ru.rastorguev.tm.entity.Project;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class ProjectController {
    private static List<Project> projects = new LinkedList<Project>();

    Project getProject (int Id) {
        return projects.get(Id);
    }

    public void createProject(BufferedReader reader) throws IOException {
        System.out.println("Project create");
        System.out.println("Enter name");
        projects.add(new Project(reader.readLine()));
        System.out.println("OK");
    }

    public void listProjects(BufferedReader reader) throws IOException {
        System.out.println("Project list");
        System.out.println("Enter correct ID or ALL for the full list of projects");
        if (reader.readLine().equals("ALL"))  {
            printList();
        } else {
            projects.get(Integer.parseInt(reader.readLine()));
        }
    }

    public void editProject(BufferedReader reader) throws IOException {
        System.out.println("Project edit");
        System.out.println("Enter ID");
        printList();
        int projectId = Integer.parseInt(reader.readLine());
        System.out.println("Enter new name");
        projects.get(projectId).setName(reader.readLine());
    }

    public void removeProject(BufferedReader reader) throws IOException {
        System.out.println("Project remove");
        System.out.println("Enter ID");
        int projectId = Integer.parseInt(reader.readLine());
        projects.remove(projectId - 1);
        System.out.println("Project with ID " + projectId + " removed");
    }

    public void clearProjects() {
        projects.clear();
        System.out.println("All project removed");
    }

    public static void printList() {
        for (int i = 0; i < projects.size(); i++) {
            System.out.println((i+1) + "." + projects.get(i).getName());
        }
    }

}
