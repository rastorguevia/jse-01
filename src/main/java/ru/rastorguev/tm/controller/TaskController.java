package ru.rastorguev.tm.controller;

import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class TaskController {
    private ProjectController projectController = new ProjectController();

    public void createTask(BufferedReader reader) throws IOException {
        System.out.println("Task create");
        System.out.println("Enter Project ID");
        projectController.printList();
        int projectId = Integer.parseInt(reader.readLine()) - 1;
        System.out.println("Enter task name");
        Project project = projectController.getProject(projectId);
        project.getTasks().add(new Task(reader.readLine(), project));
        System.out.println("OK");
    }

    public void listTasks(BufferedReader reader) throws IOException {
        System.out.println("Task list");
        System.out.println("Enter Project ID");
        projectController.printList();
        int projectId = Integer.parseInt(reader.readLine()) - 1;
        Project project = projectController.getProject(projectId);
        List<Task> tasks= project.getTasks();
        System.out.println("Enter correct Task ID or ALL for the full list of Tasks");
        if (reader.readLine().equals("ALL")) {
           printTaskList(tasks);
        } else {
            tasks.get(Integer.parseInt(reader.readLine()));
        }
    }

    public void editTask(BufferedReader reader) throws IOException {
        System.out.println("Task edit");
        System.out.println("Enter Project ID");
        projectController.printList();
        int projectId = Integer.parseInt(reader.readLine());
        Project project = projectController.getProject(projectId);
        List<Task> tasks= project.getTasks();
        System.out.println("Enter Task ID");
        printTaskList(tasks);
        int taskId = Integer.parseInt(reader.readLine()) - 1;
        System.out.println("Enter new Task name");
        project.getTasks().get(taskId).setName(reader.readLine());
        System.out.println("OK");
    }

    public void removeTask(BufferedReader reader) throws IOException {
        System.out.println("Task remove");
        System.out.println("Enter Project ID");
        projectController.printList();
        int projectId = Integer.parseInt(reader.readLine()) - 1;
        Project project = projectController.getProject(projectId);
        List<Task> tasks= project.getTasks();
        System.out.println("Enter Task ID");
        printTaskList(tasks);
        int taskId = Integer.parseInt(reader.readLine()) - 1;
        project.getTasks().remove(taskId);
        System.out.println("Task removed");
    }

    public void clearTask(BufferedReader reader) throws IOException {
        System.out.println("Enter Project ID");
        projectController.printList();
        int projectId = Integer.parseInt(reader.readLine()) - 1;
        Project project = projectController.getProject(projectId);
        project.getTasks().clear();
        System.out.println("All tasks removed");
    }

    public void printTaskList(List<Task> tasks) {
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println((i + 1) + "." + tasks.get(i).getName());
        }
    }
}

